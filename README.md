###Scala PaaS Cloud - run scala/play apps in cloud in few clicks


## libs
- play (web framework)
- reactive-docker (docker client api)
- play-json4s (json handling at play with futures)
- scalate (jade templates)
- securesocial (social logins)

## inspiration
- scala-gitlab-api (gitlab api client)
